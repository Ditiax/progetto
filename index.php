<?php
session_start();

if (! isset($_SESSION['username'])) {
    $_SESSION['msg'] = "Devi prima fare il login!";
    header('location: login.php');
}

if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    header("location: login.php");
}

?>
<!DOCTYPE html>
<html>
<head>
<title>Home</title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="header">
		<h2>Home Page</h2>
	</div>
	<div class="content">

		<!-- notification message -->
		<?php if (isset($_SESSION['success'])) : ?>
			<div class="error success">
			<h3>
					<?php
    echo $_SESSION['success'];
    unset($_SESSION['success']);
    ?>
				</h3>
		</div>
		<?php endif ?>

		<!-- logged in user information -->
		<?php  if (isset($_SESSION['username'])) : ?>
			<p>
			Benvenuto <strong><?php echo $_SESSION['username']; ?></strong>
		</p>
		<p>
			<a href="index1.php?continue='1'">Continua</a>
		</p>
		<p>
			<a href="login.php?return to login page='1'">Torna alla pagina di
				login</a>
		</p>
		<?php endif ?>
	</div>

</body>
</html>
