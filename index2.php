<?php
include ('server.php');
$uname = $_SESSION['username'];
$adm = 0;
if ($uname == "axel") {
    $adm = 1;
}
if (isset($_GET['edit'])) {
    $Id_Q = $_GET['edit'];
    $update = true;
    $record = mysqli_query(Conn(), "SELECT * FROM question WHERE Id_Q=$Id_Q");
    $n = mysqli_fetch_array($record);
    $na = $n['Text_Q'];
}
?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="style1.css">
<title>Home</title>
</head>

<body>
	<div class="container">
		<div style="text-decoration: underline" class="topright">
			<b><a href="login.php">Logout</a></b>
		</div>
	</div>
	<div id="header">
		<div class="help_header">
			<span style="color: white">Hai accesso con username <?php echo "<b>".$uname."</b>" ?></span>
		</div>
	</div>
	<div style="text-decoration: underline" class="toprightcorner">
		<b><a href="index1.php">Home</a></b>
	</div>
	<?php if (isset($_SESSION['message'])): ?>
		<div class="msg">
			<?php
    echo $_SESSION['message'];
    unset($_SESSION['message']);
    ?>
		</div>
	<?php endif ?>

<?php $results = mysqli_query(Conn(), "SELECT * FROM question"); ?>

<table>
		<thead>
			<tr>
				<th>Name</th>
				<th>Question</th>
				<th colspan="2">Action</th>
			</tr>
		</thead>
	
	<?php while ($row = mysqli_fetch_array($results)) { ?>
		<tr>
			<td><?php echo $row['name']; ?></td>
			<td><?php echo $row['Text_Q']; ?></td>
			<td>
				<?php if($uname==$row['name']){?>
						<a href="index2.php?edit=<?php echo $row['Id_Q']; ?>"
				class="edit_btn">Edita</a>
				<?php } ?>					
			</td>
			<td>
				<?php if(($uname==$row['name'])||($adm==1)){ ?>
						<a href="server.php?del=<?php echo $row['Id_Q']; ?>"
				class="del_btn">Elimina</a>
				<?php } ?>
			</td>
		</tr>
	<?php } ?>
</table>



	<form method="post" action="server.php">

		<input type="hidden" name="Id_Q" value="<?php echo $Id_Q; ?>">

		<div class="input-group">
			<label>Nome</label> <input readonly name="name"
				value="<?php echo $uname; ?>">
		</div>

		<div class="input-group">
			<label>Domanda</label> <input type="text" name="Text_Q"
				value="<?php echo $na; ?>">
		</div>
		<div class="input-group">

		<?php if ($update == true): ?>
			<button class="btn" type="submit" name="update">Aggiorna</button>
		<?php else: ?>
			<button class="btn" type="submit" name="save">Inserisci</button>
		<?php endif ?>
	</div>
	</form>
</body>
</html>