<?php
include ('server.php');
$uname = $_SESSION['username'];
$adm = 0;
if ($uname == "axel") {
    $adm = 1;
}
if (isset($_GET['rep'])) {
    $Id_Q = $_GET['rep'];
    $record = mysqli_query(Conn(), "SELECT * FROM question WHERE Id_Q=$Id_Q");
    $n = mysqli_fetch_array($record);
    $q = $n['Text_Q'];
    $na = $n['name'];
    $results = mysqli_query(Conn(), "SELECT * FROM reply WHERE tQ_R='$q'");
}
?>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" type="text/css" href="style1.css">
<title>Home</title>
</head>
<body>

	<div class="container">

		<div style="text-decoration: underline" class="topright">
			<b><a href="login.php">Logout</a></b>
		</div>

	</div>
	<div id="header">
		<div class="help_header">
			<span style="color: white">Hai accesso con username <?php echo "<b>".$uname."</b>" ?></span>
		</div>
	</div>
	<div style="text-decoration: underline" class="toprightcorner">
		<b><a href="index1.php">Home</a></b>
	</div>
	<?php if (isset($_SESSION['message'])){ ?>
		<div class="msg">
			<?php
    echo $_SESSION['message'];
    unset($_SESSION['message']);
    ?>
		</div>
	<?php } ?>



<table>
		<thead>
			<tr>
				<th>Questioner</th>
				<th>Question</th>
				<th>Replier</th>
				<th>Reply</th>
				<th style="text-align: center" colspan="2">Action</th>
			</tr>
		</thead>
		<tbody>

<?php
while ($row = mysqli_fetch_array($results)) {
    ?>
			<tr>
				<td><?php echo $row['userQuestion']; ?></td>
				<td><?php echo $row['tQ_R']; ?></td>
				<td><?php echo $row['userReply']; ?></td>
				<td><?php echo $row['Text_R']; ?></td>
				<td>
					<?php if($uname==$row['userReply']){?>
							<a href="index5.php?edit1=<?php echo $row['Id_R']; ?>"
					class="edit_btn">Edita</a>
					<?php } ?>
						
				</td>
				<td>
					<?php if(($uname==$row['userReply'])||($adm==1)){ ?>
							<a href="server.php?del1=<?php echo $row['Id_R']; ?>"
					class="del_btn">Elimina</a>
					<?php } ?>
				</td>
			</tr>
	<?php
}
?>
</tbody>
	</table>
	

<?php

if (isset($_GET['rep'])) {
    ?>
<form method="post" action="server.php">

		<input type="hidden" name="Id_R" value="<?php echo $na; ?>">

		<div class="input-group">
			<label>Questioner</label> <input readonly name="userQuestion"
				value="<?php echo $na; ?>">
		</div>
		<div class="input-group">
			<label>Question</label> <input readonly name="tQ_R"
				value="<?php echo $q; ?>">
		</div>
		<div class="input-group">
			<label>Replier</label> <input readonly name="userReply"
				value="<?php echo $uname; ?>">
		</div>
		<div class="input-group">
			<label>Reply</label> <input type="text" name="Text_R">
		</div>
		<div class="input-group">
			<button class="btn" type="submit" name="reply"
				style="background: #FF7F50;">Rispondi</button>
		</div>
	</form>
<?php } ?>
</body>
</html>