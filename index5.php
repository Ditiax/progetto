
<?php
include ('server.php');
$uname = $_SESSION['username'];
$risp = "";
if (isset($_GET['edit1'])) {
    $Id_R = $_GET['edit1'];
    $update = true;
    $record = mysqli_query(Conn(), "SELECT * FROM reply WHERE Id_R=$Id_R");
    $n = mysqli_fetch_array($record);
    $q = $n['tQ_R'];
    $na = $n['userQuestion'];
}
?>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" type="text/css" href="style1.css">
<title>Home</title>
</head>
<body>

	<div class="container">

		<div style="text-decoration: underline" class="topright">
			<b><a href="login.php">Logout</a></b>
		</div>

	</div>
	<div id="header">
		<div class="help_header">
			<span style="color: white">Hai accesso con username <?php echo "<b>".$uname."</b>" ?></span>
		</div>
	</div>
	<div style="text-decoration: underline" class="toprightcorner">
		<b><a href="index1.php">Home</a></b>
	</div>
	<?php if (isset($_SESSION['message'])): ?>
		<div class="msg">
			<?php
    echo $_SESSION['message'];
    unset($_SESSION['message']);
    ?>
		</div>
	<?php endif ?>

<form method="post" action="server.php">

		<input type="hidden" name="Id_R" value="<?php echo $Id_R; ?>">

		<div class="input-group">
			<label>Questioner</label> <input readonly name="userQuestion"
				value="<?php echo $na; ?>">
		</div>
		<div class="input-group">
			<label>Question</label> <input readonly name="tQ_R"
				value="<?php echo $q; ?>">
		</div>
		<div class="input-group">
			<label>Replier</label> <input readonly name="userReply"
				value="<?php echo $uname; ?>">
		</div>
		<div class="input-group">
			<label>Reply</label> <input type="text" name="Text_R"
				value="<?php echo $risp; ?>">
		</div>
		<div class="input-group">
		<?php if ($update == true): ?>
			<button class="btn" type="submit" name="updaterep"
				style="background: #FF7F50;">Aggiorna</button>
		<?php else: ?>
			<button class="btn" type="submit" name="save">Inserisci</button>
		<?php endif ?>
	</div>
	</form>
</body>
</html>