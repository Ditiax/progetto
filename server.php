<?php
session_start();
// variable declaration
$username = "";
$email = "";
$errors = array();
$_SESSION['success'] = "";
$userQuestion = "";
$tQ_R = "";
$userReply = "";
$Text_R = "";
$name = "";
$Text_Q = "";
$Id_Q = 0;
$Id_R = 0;


// connect to database
function Conn()
{
    $db = mysqli_connect('localhost', 'root', '', 'isa');
    return $db;
}

// REGISTER USER
if (isset($_POST['reg_user'])) {
    // receive all input values from the form
    $username = mysqli_real_escape_string(Conn(), $_POST['username']);
    $email = mysqli_real_escape_string(Conn(), $_POST['email']);
    $password_1 = mysqli_real_escape_string(Conn(), $_POST['password_1']);
    $password_2 = mysqli_real_escape_string(Conn(), $_POST['password_2']);

    // form validation: ensure that the form is correctly filled
    if (empty($username)) {
        array_push($errors, "Username is required");
    } else {
        $result = mysqli_query(Conn(), "SELECT * FROM user WHERE username='$username'");
        $res = mysqli_fetch_array($result);
        if (strcmp($res['username'], $username) == 0) {
            array_push($errors, "Username not available");
        }
    }
    if (empty($email)) {
        array_push($errors, "Email is required");
    } else {
        $result = mysqli_query(Conn(), "SELECT * FROM user WHERE email='$email'");
        $res = mysqli_fetch_array($result);
        if (strcmp($res['email'], $email) == 0) {
            array_push($errors, "Email not available");
        }
    }
    if (empty($password_1)) {
        array_push($errors, "Password is required");
    }

    if ($password_1 != $password_2) {
        array_push($errors, "The two passwords do not match");
    }

    // register user if there are no errors in the form
    if (count($errors) == 0) {
        $password = $password_1; // encrypt the password before saving in the database
        $query = "INSERT INTO user (username, email, password) 
					  VALUES('$username', '$email', '$password')";
        mysqli_query(Conn(), $query);

        $_SESSION['username'] = $username;
        $_SESSION['success'] = "Complimenti! Ora sei un nuovo utente del sito";
        header('location: index.php');
    }
}

// LOGIN USER
if (isset($_POST['login_user'])) {
    $username = mysqli_real_escape_string(Conn(), $_POST['username']);
    $password = mysqli_real_escape_string(Conn(), $_POST['password']);

    if (empty($username)) {
        array_push($errors, "Il campo username non e' compilato");
    }
    if (empty($password)) {
        array_push($errors, "Il campo password non e' compilato");
    }

    if (count($errors) == 0) {
        $query = "SELECT * FROM user WHERE username='$username' AND password='$password'";
        $results = mysqli_query(Conn(), $query);

        if (mysqli_num_rows($results) == 1) {
            $_SESSION['username'] = $username;
            $_SESSION['success'] = "Ora puoi procedere nel sito";
            header('location: index.php');
        } else {
            array_push($errors, "La combinazione username/password e' errata");
        }
    }
}

if (isset($_POST['save'])) {
    $name = $_POST['name'];
    $Text_Q = $_POST['Text_Q'];

    mysqli_query(Conn(), "INSERT INTO question (name, Text_Q) VALUES ('$name', '$Text_Q')");
    $_SESSION['message'] = "Domanda salvata";
    header('location: index1.php');
}

if (isset($_POST['update'])) {
    $Id_Q = $_POST['Id_Q'];
    $name = $_POST['name'];
    $Text_Q = $_POST['Text_Q'];
    
    mysqli_query(Conn(), "UPDATE question SET name='$name', Text_Q='$Text_Q' WHERE Id_Q=$Id_Q");
    $_SESSION['message'] = "Domanda aggiornata";
    header('location: index1.php');
}

if (isset($_POST['updaterep'])) {
    $Id_R = $_POST['Id_R'];
    $userQuestion = $_POST['userQuestion'];
    $tQ_R = $_POST['tQ_R'];
    $userReply = $_POST['userReply'];
    $Text_R = $_POST['Text_R'];
    mysqli_query(Conn(), "UPDATE reply SET userQuestion='$userQuestion', tQ_R='$tQ_R', userReply='$userReply', Text_R='$Text_R' WHERE Id_R=$Id_R");
    $_SESSION['message'] = "Risposta aggiornata";
    header('location: index4.php');
}

if (isset($_POST['reply'])) {
    $userQuestion = $_POST['userQuestion'];
    $tQ_R = $_POST['tQ_R'];
    $userReply = $_POST['userReply'];
    $Text_R = $_POST['Text_R'];
    mysqli_query(Conn(), "INSERT INTO reply (userQuestion, tQ_R, userReply, Text_R) VALUES ('$userQuestion', '$tQ_R','$userReply','$Text_R')");
    $_SESSION['message'] = "Risposta inserita";
    header('location: index1.php');
}

if (isset($_GET['del'])) {
    $Id_Q = $_GET['del'];
    mysqli_query(Conn(), "DELETE FROM question WHERE Id_Q='$Id_Q'");
    mysqli_query(Conn(), "DELETE FROM reply WHERE '$tQ_R' = '$Text_Q'");
    $_SESSION['message'] = "Domanda cancellata";
    header('location: index1.php');
}

if (isset($_GET['del1'])) {
    $Id_R = $_GET['del1'];
    mysqli_query(Conn(), "DELETE FROM reply WHERE Id_R=$Id_R");
    $_SESSION['message'] = "Risposta cancellata";
    header('location: index4.php');
}

$_SESSION['Text_Q'] = $Text_Q;

$results = mysqli_query(Conn(), "SELECT * FROM question");

?>